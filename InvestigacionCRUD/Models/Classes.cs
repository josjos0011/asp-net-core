using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace InvestigacionCRUD.Models
{
    /// <summary>
    /// Modelo de datos para un usuario.
    /// </summary>
    public class User
    {
        public long? Id { get; set; } // Identificador único del usuario
        public string? Name { get; set; } // Nombre del usuario
        public string? Cedula { get; set; } // Cédula del usuario
        public int Age { get; set; } // Edad del usuario
        public string? Email { get; set; } // Correo electrónico del usuario
        public List<Task>? Tasks { get; set; } // Lista de tareas asignadas al usuario
    }

    /// <summary>
    /// Modelo de datos para una tarea.
    /// </summary>
    public class Task
    {
        public int Id { get; set; } // Identificador único de la tarea

        [Required(ErrorMessage = "El título es requerido")]
        public string? Title { get; set; } // Título de la tarea

        [Required(ErrorMessage = "La descripción es requerida")]
        public string? Description { get; set; } // Descripción de la tarea

        [Required(ErrorMessage = "La fecha de vencimiento es requerida")]
        public DateTime? DueDate { get; set; } // Fecha de vencimiento de la tarea

        [Required(ErrorMessage = "Las horas son requeridas")]
        public int Hours { get; set; } // Horas requeridas para completar la tarea

        [Required(ErrorMessage = "La ubicación es requerida")]
        public string? Location { get; set; } // Ubicación de la tarea

        [Required(ErrorMessage = "La prioridad es requerida")]
        public string? Priority { get; set; } // Prioridad de la tarea

        [Required(ErrorMessage = "El estado de la tarea es requerido")]
        public string? TaskProgress { get; set; } // Estado actual de la tarea

        public string? PhotoUrl { get; set; } // URL de la foto asociada a la tarea

        [Required(ErrorMessage = "El usuario asignado es requerido")]
        public int UserId { get; set; } // Identificador del usuario asignado a la tarea

        public bool IsReady { get; set; } = false; // Indica si la tarea está completada

        public User User { get; set; } // Usuario asignado a la tarea
    }
}
