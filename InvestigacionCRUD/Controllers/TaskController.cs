using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using InvestigacionCRUD.Models;

namespace InvestigacionCRUD.Controllers
{
    /// <summary>
    /// Controlador para gestionar las operaciones relacionadas con las tareas.
    /// </summary>
    public class TaskController : Controller
    {
        private readonly IHttpClientFactory _clientFactory;
        private readonly string _baseUrl = "http://localhost:8080";

        public TaskController(IHttpClientFactory clientFactory)
        {
            _clientFactory = clientFactory;
        }

        // Método genérico para obtener la respuesta de la API de forma asíncrona
        private async Task<T> GetApiResponseAsync<T>(string relativeUrl)
        {
            using var client = _clientFactory.CreateClient();
            var response = await client.GetAsync(_baseUrl + relativeUrl);

            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                return JsonSerializer.Deserialize<T>(content, new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                });
            }

            throw new HttpRequestException($"Error: {response.StatusCode}");
        }

        // Vista para mostrar la lista de tareas
        public async Task<IActionResult> TaskIndex()
        {
            try
            {
                var users = await GetApiResponseAsync<List<User>>("/user/list");
                return View(users);
            }
            catch (HttpRequestException ex)
            {
                ViewBag.Message = ex.Message;
                return View(new List<User>());
            }
        }

        // Vista para agregar una nueva tarea
        public async Task<IActionResult> TaskAdd()
        {
            var users = await GetApiResponseAsync<List<User>>("/user/list");
            ViewBag.Users = users;
            return View();
        }

        // Método para agregar una nueva tarea
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> TaskAdd(int userId, InvestigacionCRUD.Models.Task task)
        {
            var url = $"/task/create/{userId}";
            using var client = _clientFactory.CreateClient();
            var response = await client.PostAsJsonAsync(_baseUrl + url, task);

            if (response.IsSuccessStatusCode)
            {
                return RedirectToAction(nameof(TaskIndex));
            }

            throw new HttpRequestException($"Error: {response.StatusCode}");
        }

        // Método para eliminar una tarea
[HttpDelete]
public async Task<IActionResult> DeleteTask(int id)
{
    var url = $"/task/delete/{id}";
    using var client = _clientFactory.CreateClient();
    var response = await client.DeleteAsync(_baseUrl + url);

    if (response.IsSuccessStatusCode)
    {
        return Ok(new { success = true });
    }

    return StatusCode((int)response.StatusCode, new { success = false, message = response.ReasonPhrase });
}



        // Vista para mostrar los detalles de una tarea
        public async Task<IActionResult> GetById(int id)
        {
            var relativeUrl = $"/task/{id}";
            var task = await GetApiResponseAsync<InvestigacionCRUD.Models.Task>(relativeUrl);

            if (task != null)
            {
                return View(task);
            }
            else
            {
                return NotFound();
            }
        }

        // Método para actualizar una tarea
        [HttpPatch]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> TaskUpdate(int id, InvestigacionCRUD.Models.Task taskDetails)
        {
            var url = $"/task/update/{id}";
            using var client = _clientFactory.CreateClient();
            var response = await client.PatchAsJsonAsync(_baseUrl + url, taskDetails);

            if (response.IsSuccessStatusCode)
            {
                return RedirectToAction(nameof(TaskIndex));
            }

            throw new HttpRequestException($"Error: {response.StatusCode}");
        }
    }
}
