using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using InvestigacionCRUD.Models;

namespace InvestigacionCRUD.Controllers
{
    /// <summary>
    /// Controlador para gestionar las operaciones relacionadas con los usuarios.
    /// </summary>
    public class UserController : Controller
    {
        private readonly IHttpClientFactory _clientFactory;
        private readonly string _baseUrl = "http://localhost:8080";

        public UserController(IHttpClientFactory clientFactory)
        {
            _clientFactory = clientFactory;
        }

        // Método genérico para obtener la respuesta de la API de forma asíncrona
        private async Task<T> GetApiResponseAsync<T>(string relativeUrl)
        {
            using var client = _clientFactory.CreateClient();
            var response = await client.GetAsync(_baseUrl + relativeUrl);

            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                return JsonSerializer.Deserialize<T>(content, new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                });
            }

            throw new HttpRequestException($"Error: {response.StatusCode}");
        }

        // Vista para mostrar la lista de usuarios
        public async Task<IActionResult> Index()
        {
            try
            {
                var users = await GetApiResponseAsync<List<User>>("/user/list");
                return View(users);
            }
            catch (HttpRequestException ex)
            {
                ViewBag.Message = ex.Message;
                return View(new List<User>());
            }
        }
    }
}
