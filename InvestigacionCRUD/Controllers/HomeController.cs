using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using InvestigacionCRUD.Models;

namespace InvestigacionCRUD.Controllers;

public class HomeController : Controller
{
    public IActionResult Index()
    {
        return View();
    }
}
