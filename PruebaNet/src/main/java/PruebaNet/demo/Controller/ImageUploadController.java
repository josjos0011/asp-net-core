
package PruebaNet.demo.Controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@RestController
@RequestMapping("/api/images")
public class ImageUploadController {

    @Value("${image.upload.dir}")
    private String uploadDir;

    @PostMapping("/upload")
    public String uploadImage(@RequestParam("file") MultipartFile file) {
        try {
            
            // Crear el directorio si no existe
            File directory = new File(uploadDir);
            if (!directory.exists()) {
                directory.mkdirs();
            }

            // Guardar el archivo
            String fileName = file.getOriginalFilename();
            Path filePath = Paths.get(uploadDir, fileName);
            
            // Normalizar la ruta para usar barras invertidas simples
            String normalizedPath = filePath.toString().replace("\\", "/");
            // Guardar el archivo con la ruta normalizada
            Files.write(Paths.get(normalizedPath), file.getBytes());
            
            // Devolver una ruta relativa dentro de la aplicación
            String relativePath = "static/images/" + fileName;

            return relativePath;
            
        } catch (IOException e) {
            e.printStackTrace();
            return "Error al subir la imagen";
        }
    }

}
