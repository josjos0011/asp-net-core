package PruebaNet.demo.Controller;

import PruebaNet.demo.Domain.User;
import PruebaNet.demo.JPA.UserServiceJPA;
import PruebaNet.demo.JPA.TaskServiceJPA;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Optional;

@RestController
@RequestMapping("user")
@CrossOrigin(origins = "http://localhost:5099") // Permitir solicitudes desde el frontend en este origen
public class UserController {

    @Autowired
    private UserServiceJPA service;

    @Autowired
    private TaskServiceJPA taskService;

    @GetMapping("/list")
    public ResponseEntity<ArrayList<User>> list() {
        ArrayList<User> list = (ArrayList<User>) service.listActiveUsers();
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @GetMapping("/get/{id}")
    public ResponseEntity<User> getById(@PathVariable int id) {
        Optional<User> user = Optional.ofNullable(service.byId(id));
        return user.map(value -> new ResponseEntity<>(value, HttpStatus.OK))
                   .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping("/create")
    public ResponseEntity<ArrayList<User>> create(@RequestBody User user) {
        service.save(user);
        return list();
    }

    @PatchMapping("/update/{id}")
    public ResponseEntity<ArrayList<User>> update(@PathVariable int id, @RequestBody User userDetails) {
        Optional<User> optionalUser = Optional.ofNullable(service.byId(id));
        if (optionalUser.isPresent()) {
            User user = optionalUser.get();

            if (userDetails.getName() != null) {
                user.setName(userDetails.getName());
            }

            // Agrega otras actualizaciones de campos según sea necesario

            service.save(user);
            return list();
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<ArrayList<User>> delete(@PathVariable int id) {
        try {
            service.delete(id);
            return list();
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
