package PruebaNet.demo.Controller;

import PruebaNet.demo.Domain.Task;
import PruebaNet.demo.JPA.TaskServiceJPA;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/task")
@CrossOrigin(origins = "http://localhost:5069") // Permitir solicitudes desde el frontend en este origen
public class TaskController {

    @Autowired
    private TaskServiceJPA taskService;

    @GetMapping("/list")
    public ResponseEntity<List<Task>> listTasks() {
        List<Task> list = taskService.list();
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @PostMapping("/create/{userId}")
    public ResponseEntity<List<Task>> create(@PathVariable int userId, @RequestBody Task task) {
        taskService.save(userId, task);
        return listTasks();

    }

    @PatchMapping("/update/{id}")
    public ResponseEntity<List<Task>> update(@PathVariable int id, @RequestBody Task taskDetails) {
        Optional<Task> optionalTask = Optional.ofNullable(taskService.byId(id));
        if (optionalTask.isPresent()) {
            Task task = optionalTask.get();

            if (taskDetails.getTaskProgress() != null) {
                task.setTaskProgress(taskDetails.getTaskProgress());
            }

            if (taskDetails.getIsReady() != null) {
                task.setIsReady(taskDetails.getIsReady());
            }

            // Agrega otras actualizaciones de campos según sea necesario
            taskService.save(task.getUser().getId(), task);

            return listTasks();
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<List<Task>> delete(@PathVariable int id) {
        Optional<Task> optionalTask = Optional.ofNullable(taskService.byId(id));
        if (optionalTask.isPresent()) {
            taskService.delete(id);
            return listTasks();
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<Task> byID(@PathVariable int id) {
        Optional<Task> optionalTask = Optional.ofNullable(taskService.byId(id));
        if (optionalTask.isPresent()) {
            return new ResponseEntity<>(optionalTask.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

}
