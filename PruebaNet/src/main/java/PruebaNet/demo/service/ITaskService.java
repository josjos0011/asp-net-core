/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package PruebaNet.demo.service;

import PruebaNet.demo.Domain.Task;
import java.util.List;






public interface ITaskService {

void save(int userId,Task task);
List<Task> list();
void delete(int taskID);
Task byId(int taskID);
void update(int id, Task taskDetails);

}
