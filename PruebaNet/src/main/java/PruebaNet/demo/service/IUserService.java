/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package PruebaNet.demo.service;

import PruebaNet.demo.Domain.Task;
import PruebaNet.demo.Domain.User;
import java.util.List;






public interface IUserService {

void save(User user);
List<User> list();
void delete(int userID);
User byId(int userID);
void Update (int userId, User userDetails);

List<User> listActiveUsers();
}

