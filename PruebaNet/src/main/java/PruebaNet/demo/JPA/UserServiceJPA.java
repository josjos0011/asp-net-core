package PruebaNet.demo.JPA;

import PruebaNet.demo.Domain.User;
import PruebaNet.demo.repository.UserRepository;
import PruebaNet.demo.service.IUserService;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Primary
public class UserServiceJPA implements IUserService {

    @Autowired
    private UserRepository repo;

    @Override
    public void save(User user) {
        repo.save(user);
    }

    @Override
    public List<User> list() {
        return repo.findAll();
    }

    @Override
    public void delete(int userId) {
        repo.deleteById(userId);
    }

    @Override
    public User byId(int userId) {
        return repo.findById(userId).orElse(null);
    }

    @Override
    public void Update(int userId, User userDetails) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List<User> listActiveUsers() {
        return repo.getAllUsers();
    }
    




    
}
