package PruebaNet.demo.JPA;

import PruebaNet.demo.Domain.Task;
import PruebaNet.demo.Domain.User;
import PruebaNet.demo.repository.TaskRepository;
import PruebaNet.demo.service.ITaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Primary
public class TaskServiceJPA implements ITaskService {

    @Autowired
    private TaskRepository repo;
    
    @Autowired
    private UserServiceJPA userService;

    @Override
    public void save(int userId, Task task) {
        User u = userService.byId(userId);
        task.setUser(u);
        repo.save(task);
    }

    @Override
    public List<Task> list() {
        return repo.findAll();
    }

    @Override
    public void delete(int taskId) {
        repo.deleteById(taskId);
    }

    @Override
    public Task byId(int taskId) {
        return repo.findById(taskId).orElse(null);
    }

    @Override
    public void update(int id, Task taskDetails) {
        repo.save(taskDetails);
    }
}
