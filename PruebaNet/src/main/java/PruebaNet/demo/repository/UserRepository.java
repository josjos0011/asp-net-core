/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package PruebaNet.demo.repository;


import PruebaNet.demo.Domain.User;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;



public interface UserRepository extends JpaRepository<User, Integer>{
    
    
    // Define el método para llamar al procedimiento almacenado getAllUsers
    @Query(nativeQuery = true, value = "CALL getAllUsers()")
    List<User> getAllUsers();
    
    
}
