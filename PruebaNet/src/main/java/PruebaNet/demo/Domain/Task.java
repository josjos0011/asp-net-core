package PruebaNet.demo.Domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

@Entity
@Table(name = "task")
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = false, length = 255)
    private String title;

    @Column(columnDefinition = "TEXT")
    private String description;

    @Temporal(TemporalType.DATE)
    @Column(name = "due_date")
    @DateTimeFormat(iso = ISO.DATE)
    private Date dueDate;

    @Column(name = "task_progress", columnDefinition = "CHAR(25) DEFAULT 'PENDING'")
    private String taskProgress = "PENDING";

    @Column(name = "priority", columnDefinition = "CHAR(15) DEFAULT 'LOW'")
    private String priority = "LOW";
    
    
    @Column(name = "photo_url", length = 999)
    private String photoUrl;

    @Column(name = "hours")
    private int hours;
    
    private String location;

    @Column(name = "is_ready", columnDefinition = "char(1) DEFAULT '0'")
    private Boolean isReady;

    @JsonBackReference
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    // Constructors, getters, and setters
    public Task() {
    }

    public Task(int id, String title, String description, Date dueDate,String location,String taskProgress, String priority, String photoUrl, int hours, Boolean isReady, User user) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.dueDate = dueDate;
        this.location = location;
        this.taskProgress = taskProgress;
        this.priority = priority;
        this.photoUrl = photoUrl;
        this.hours = hours;
        this.isReady = isReady;
        this.user = user;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    
    public String getTaskProgress() {
        return taskProgress;
    }

    public void setTaskProgress(String taskProgress) {
        this.taskProgress = taskProgress;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public Boolean getIsReady() {
        return isReady;
    }

    public void setIsReady(Boolean isReady) {
        this.isReady = isReady;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }


}
